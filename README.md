# Talking MPC

Here lies a somewhat maintained and organized collection of topics, papers,
ideas, and discussions about [MPC ((Secure) Multi-Party
Computation)](https://en.wikipedia.org/wiki/Secure_multi-party_computation) and
it's related areas.

## Courses

- [Crypto intro course by Randy Schull](https://cs.wellesley.edu/~cs310/) (some good slides in here, undergrad level)
- [Secure Computation by Sanjam Garg](https://people.eecs.berkeley.edu/~sanjamg/classes/cs294-spring16/)
- [Simons Cryptography Boot Camp 2015](https://simons.berkeley.edu/workshops/crypto2015-boot-camp)
- [The 5th BIU Winter School (2015)](https://cyber.biu.ac.il/event/the-5th-biu-winter-school/)
    - [Session 1: Definitions and Oblivious Transfer](https://cyber.biu.ac.il/wp-content/uploads/2017/01/1-1.pdf)
    - [Session 2: The Yao and BMR Protocols for Secure Computation](https://cyber.biu.ac.il/wp-content/uploads/2017/01/2-1.pdf)
    - [Session 3: The GMW and BMR Multi-Party Protocols](https://cyber.biu.ac.il/wp-content/uploads/2017/01/3-1.pdf)
    - [Session 4: Efficient Zero Knowledge](https://cyber.biu.ac.il/wp-content/uploads/2017/01/4-1.pdf)
    - [Session 5: Security against Malicious Adversaries](https://cyber.biu.ac.il/wp-content/uploads/2017/01/5-1.pdf)
    - ...
- [Graduate Theory of Computation by Daniel Wichs](https://piazza.com/northeastern/spring2018/cs7805/home)

## Papers

### Proofs
[2016/046](https://eprint.iacr.org/2016/046) ([pdf](https://eprint.iacr.org/2016/046.pdf))\
How To Simulate It - A Tutorial on the Simulation Proof Technique\
*Yehuda Lindell*

#### Universally Composability (UC)
[2000/067](https://eprint.iacr.org/2000/067) ([pdf](https://eprint.iacr.org/2000/067.pdf))\
Universally composable security: A new paradigm for cryptographic protocols\
*Ran Canetti*

[2002/140](https://eprint.iacr.org/2000/067) ([pdf](https://eprint.iacr.org/2002/140.pdf))\
Universally Composable Two-Party and Multi-Party Secure Computation\
*Ran Canetti and Yehuda Lindell and Rafail Ostrovsky and Amit Sahai*

### Protocols
[SFCS'82/160](https://dl.acm.org/citation.cfm?id=1382751) ([pdf](https://research.cs.wisc.edu/areas/sec/yao1982-ocr.pdf))\
Protocols for Secure Computations\
*Andrew Yao*

[2019/706](https://eprint.iacr.org/2019/706) ([pdf](https://eprint.iacr.org/2019/706.pdf))\
Endemic Oblivious Transfer\
*Daniel Masny and Peter Rindal*

## Textbooks
[10.1201/b17668](https://www.semanticscholar.org/paper/Introduction-to-Modern-Cryptography%2C-Second-Edition-Katz-Lindell/d7f6b1d1c5dd845c897858b63bea2e4ad4f208ee) (pdf)\
Introduction to Modern Cryptography (2nd Edition)\
*Jonathan Katz & Yehuda Lindell*

[ACI](https://www.semanticscholar.org/paper/A-Course-in-Cryptography-Rafael-Pass-Abhi-Shelat/9d58a8bd8510cfb390a0a82f7e11839b22a12bd8) ([pdf](https://pdfs.semanticscholar.org/9d58/a8bd8510cfb390a0a82f7e11839b22a12bd8.pdf?_ga=2.174023523.1146905905.1568750685-939996512.1568750685))\
A Course in Cryptography\
*Rafael Pass & abhi shelat*

[SF2](https://www.seas.upenn.edu/~cis500/current/sf/plf-current/toc.html)\
Software Foundations Volume 2: Programming Language Foundations\
*Benjamin C. Pierce and Arthur Azevedo de Amorim and Chris Casinghino and Marco Gaboardi and Michael Greenberg and Cătălin Hriţcu and Vilhelm Sjöberg and Andrew Tolmach and Brent Yorgey*

## Math References
- [Reed-Solomon Codes Lecture Notes](https://www2.cs.duke.edu/courses/spring10/cps296.3/rs_scribe.pdf)
    - has quick intro to Galois fields
